#!/usr/bin/env python3
# Script servant à valider l'intégrité et la complétion des données dans numeros.yaml
import re, yaml
from pathlib import Path

SCRIPT_DIR = Path(__file__).parent
REQUIRED_PROPS = ("page", "titre", "description_courte", "dessin", "journaliste")
OPTIONAL_PROPS = ("lieux", "rubrique", "description_longue", "tags")
NUM_REQUIRED_PROPS = ("numero", "couverture", "url_boutique", "articles")

IGNORE_TAG_SUGGESTIONS = {
    "économie": [(38, 8)],
    "mer": [(40, 50)],
    "eau": [(41, 124)],
    "guerre": [(42, 112)],
    "retraite": [(45, 150)],
    "santé": [(45, 165)],
    "énergie": [(45, 190)],
}

def is_tag_matching(tag, lower_desc):
    regex = re.compile("[^a-z]" + tag + "[^a-rt-z]")
    return regex.search(lower_desc)

with (SCRIPT_DIR / "numeros.yaml").open() as numeros_file:
    numeros = yaml.safe_load(numeros_file)
ALL_TAGS = set(sum((article.get("tags", []) for numero in numeros for article in numero["articles"]), []))
print("#tags:", len(ALL_TAGS))
for numero in numeros:
    for required_prop in NUM_REQUIRED_PROPS:
        if not numero.get(required_prop):
            raise ValueError(f"Champ '{required_prop}' manquant ou vide pour le numéro {numero.get('numero', '?')}")
    if not isinstance(numero["numero"], int):
        raise TypeError(f"Champ 'numero' invalide ('{numero['numero']}') pour le numéro {numero.get('numero', '?')}")
    couv_file = Path(SCRIPT_DIR / "images" / "numeros" / numero["couverture"])
    if not couv_file.exists():
        raise FileNotFoundError(f"Fichier '{couv_file}' non trouvé pour la couvertur du numéro {numero['numero']}")
    locations_count = 0
    prev_page = None
    for article in numero["articles"]:
        where = f"l'article page {article['page']} du numéro {numero.get('numero', '?')}"
        for required_prop in REQUIRED_PROPS:
            if not article.get(required_prop):
                raise ValueError(f"Champ '{required_prop}' manquant ou vide dans {where}")
        if not isinstance(article["page"], int):
            raise TypeError(f"Champ 'page' invalide ('{article['page']}') dans {where}")
        for location in article.get("lieux", ()):
            for required_prop in ("nom", "coords"):
                if required_prop not in location:
                    raise ValueError(f"Champ '{required_prop}' manquant dans un 'lieu' de {where}")
        for tag in article.get("tags", ()):
            if tag != tag.lower():
                raise ValueError(f"Le tag '{tag}' n'est pas en minuscule dans {where}")
        for prop in article:
            if prop not in REQUIRED_PROPS + OPTIONAL_PROPS:
                raise ValueError(f"Champ '{prop}' inconnu dans {where}")
        locations_count += len(article.get("lieux", ()))
        tags = article.get("tags", [])
        description_courte = article.get("description_courte") or ""
        description_longue = article.get("description_longue") or ""
        lower_short_desc = description_courte.lower()
        lower_long_desc = description_longue.lower()
        for tag in (ALL_TAGS - set(tags)):
            if (numero['numero'], article['page']) in IGNORE_TAG_SUGGESTIONS.get(tag, ()):
                continue
            if is_tag_matching(tag, lower_short_desc):
                raise ValueError(f'Il serait utile d\'ajouter le tag "{tag}" comme il est présent dans la .description_courte dans {where}')
            if is_tag_matching(tag, lower_long_desc):
                raise ValueError(f'Il serait utile d\'ajouter le tag "{tag}" comme il est présent dans la .description_longue dans {where}')
        journaliste = article["journaliste"]
        journalistes = [journaliste] if isinstance(journaliste, str) else journaliste
        for journaliste in journalistes:
            if journaliste in description_courte:
                raise ValueError(f"Info dupliquée : le/la .journaliste est mentionné·e dans la .description_courte dans {where}")
            if journaliste in description_longue:
                raise ValueError(f"Info dupliquée : le/la .journaliste est mentionné·e dans la .description_longue dans {where}")
        dessin = article.get("dessin")
        if dessin:
            dessinateurs = [dessin] if isinstance(dessin, str) else dessin
            for dessinateur in dessinateurs:
                if dessinateur in description_courte:
                    raise ValueError(f"Info dupliquée : le/la dessinateur/trice est mentionné·e dans la .description_courte dans {where}")
                if dessinateur in description_longue:
                    raise ValueError(f"Info dupliquée : le/la dessinateur/trice est mentionné·e dans la .description_longue dans {where}")
        if prev_page and article["page"] < prev_page:
            raise ValueError(f"Ordre des articles ou numéro de page {article['page']} incorrect dans le numéro {numero['numero']}")
        prev_page = article["page"]
    if not any(article.get("rubrique") == "Face B" for article in numero["articles"]):
        raise ValueError(f"Le numéro {numero['numero']} ne contient aucun article 'Face B'")
    if not any(article.get("rubrique") == "Instantané" for article in numero["articles"]):
        raise ValueError(f"Le numéro {numero['numero']} ne contient aucun article 'Instantané'")
    if not any(article.get("rubrique") == "La revue des cinés" for article in numero["articles"]):
        raise ValueError(f"Le numéro {numero['numero']} ne contient aucun article 'La revue des cinés'")
    locations_count = sum(len(article.get("lieux", ())) for article in numero["articles"])
    if locations_count < 4:
        raise ValueError(f"Le numéro {numero['numero']} ne que contient très peu d'articles avec des .lieux")
    articles_with_tags = sum(1 for article in numero["articles"] if article.get("tags", ()))
    if articles_with_tags < 7:
        raise ValueError(f"Le numéro {numero['numero']} ne que contient trop peu d'articles avec des .tags")
