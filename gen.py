#!/usr/bin/env python3
# Script servant à générer le site web statique.
# L'option --watch permet de lancer un serveur HTTP et d'activer le hot-reload.
# Script Dependencies:
#    jinja2
#    livereload
import sys, webbrowser, yaml
from pathlib import Path
from shutil import copytree, rmtree

from jinja2 import Environment, FileSystemLoader
from livereload.server import Server

PORT = 8000
SCRIPT_DIR = Path(__file__).parent
TEMPLATES_DIR = SCRIPT_DIR / "templates"
OUT_DIR = Path("./public")  # Relative to the current dir when invoking this script

def gen_out_dir():
    rmtree(str(OUT_DIR), ignore_errors=True)
    OUT_DIR.mkdir()
    for dir in ("css", "fonts", "images", "js"):
        copytree(str(SCRIPT_DIR / dir), str(OUT_DIR / dir))
    with (SCRIPT_DIR / "numeros.yaml").open() as numeros_file:
        numeros = yaml.safe_load(numeros_file)
    env = Environment(loader=FileSystemLoader(TEMPLATES_DIR), autoescape=True)
    for tmpl_file in TEMPLATES_DIR.glob("*.html"):
        template = env.get_template(tmpl_file.name)
        html_file = OUT_DIR / tmpl_file.name
        html_file.write_text(template.render(numeros=numeros))

gen_out_dir()
if '--watch' in sys.argv:
    SERVER = Server()
    for dir in ("css", "fonts", "images", "js", "templates"):
        for filepath in (SCRIPT_DIR / dir).rglob("*.*"):
            SERVER.watch(str(filepath), gen_out_dir)
    SERVER.watch(str(SCRIPT_DIR / "numeros.yaml"), gen_out_dir)
    webbrowser.open(f'http://localhost:{PORT}')
    SERVER.serve(root=str(OUT_DIR), port=PORT)
