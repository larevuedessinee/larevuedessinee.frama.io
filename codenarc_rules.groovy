ruleset {

    ruleset('rulesets/basic.xml')

    ruleset('rulesets/braces.xml')

    ruleset('rulesets/comments.xml')

    ruleset('rulesets/concurrency.xml')

    ruleset('rulesets/convention.xml') {
        CompileStatic(enabled:false)
        MethodParameterTypeRequired(enabled:false)
        MethodReturnTypeRequired(enabled:false)
        NoDef(enabled:false)
        PublicMethodsBeforeNonPublicMethods(enabled:false)
        VariableTypeRequired(enabled:false)
        TrailingComma(enabled:false)
    }

    ruleset('rulesets/design.xml') {
        Instanceof(enabled:false)
        NestedForLoop(enabled:false)
    }

    ruleset('rulesets/dry.xml') {
        // Allowing several cases of code duplication:
        DuplicateListLiteral(enabled:false)
        DuplicateMapLiteral(enabled:false)
        DuplicateNumberLiteral(enabled:false)
        DuplicateStringLiteral(enabled:false)
    }

    ruleset('rulesets/enhanced.xml')

    ruleset('rulesets/exceptions.xml') {
        // We allow throwing runtime exceptions:
        ThrowRuntimeException(enabled:false)
    }

    ruleset('rulesets/formatting.xml') {
        BlockEndsWithBlankLine(enabled:false)
        BlockStartsWithBlankLine(enabled:false)
        ConsecutiveBlankLines(enabled:false)
        Indentation(enabled:false)
        LineLength(enabled:false)
        SpaceAfterOpeningBrace(enabled:false)
        SpaceAroundMapEntryColon(enabled:false)
        SpaceBeforeClosingBrace(enabled:false)
    }

    ruleset('rulesets/generic.xml')

    // ruleset('rulesets/grails.xml')

    ruleset('rulesets/groovyism.xml')

    ruleset('rulesets/imports.xml') {
        NoWildcardImports(enabled:false)
        UnusedImport(enabled:false)
    }

    // ruleset('rulesets/jdbc.xml')
    // ruleset('rulesets/junit.xml')

    ruleset('rulesets/logging.xml') {
        // We allow printing to stderr & stdout
        SystemErrPrint(enabled:false)
        SystemOutPrint(enabled:false)
    }

    ruleset('rulesets/naming.xml')

    ruleset('rulesets/security.xml') {
        JavaIoPackageAccess(enabled:false)
    }

    ruleset('rulesets/serialization.xml')

    ruleset('rulesets/size.xml') {
        // We disable some rules that do not really apply to Jenkins pipelines:
        AbcMetric(enabled:false)
        CrapMetric(enabled:false)
        CyclomaticComplexity(enabled:false)
        MethodCount(enabled:false)
    }

    ruleset('rulesets/unnecessary.xml')

    ruleset('rulesets/unused.xml')

}
