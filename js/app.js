new Vue({
    el: '#app',
    data: {
        mobile: false,
        newsletter: false,
    },
    watch: {
        mobile() {
            if (this.mobile) {
                document.querySelector('body').style.overflow = 'hidden';
            } else {
                document.querySelector('body').removeAttribute('style');
            }
        }
    },
    methods: {
        toggleMobileNavigation: function () {
            this.mobile = !this.mobile;
        },
        scrollTo: function (element) {
            var el = document.querySelector(element);
            window.scrollTo({
                top: el.getBoundingClientRect().top + window.pageYOffset,
                left: 0,
                behavior: 'smooth'
            })
        }
    }
})

var numerosElem = document.getElementById("numeros");
if (numerosElem) {
    var numeros = JSON.parse(numerosElem.textContent);

    var map = L.map('map').setView(/*center=*/[46.227638, 2.213749], /*zoom=*/6);
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    var markersLayer = new L.LayerGroup();	//layer contain searched elements
    map.addLayer(markersLayer);

    var controlSearch = new L.Control.Search({
        position: 'topright',
        layer: markersLayer,
        initial: false,
        zoom: 6,
        marker: false,
        propertyName: "searchable",
    });
    map.addControl(controlSearch);

    for (var numero of numeros) {
        for (var article of numero.articles) {
            var locations = article.lieux || [];
            if (!locations) continue;
            var rubrique = article.rubrique ? ` (${article.rubrique})`: '';
            var popupHtml = `<b>${article.titre}</b>${rubrique}
                <br>${article.description_courte}
                <br>🎨${article.dessin ? article.dessin + ' - ' : ''}💬${article.journaliste}
                <br><a href="${numero.url_boutique}">Numéro #${numero.numero}</a>, page ${article.page}`;
            var searchableFieldValues = [article.titre, article.description_courte, article.journaliste];
            if (article.dessin) searchableFieldValues.push(article.dessin);
            if (article.rubrique) searchableFieldValues.push(article.rubrique);
            if (article.description_longue) searchableFieldValues.push(article.description_longue);
            for (var loc of locations) {
                var searchable = searchableFieldValues.concat([loc.nom]).join(' - ');
                var marker = new L.Marker(new L.latLng(loc.coords), {searchable});
                marker.bindPopup(popupHtml);
                markersLayer.addLayer(marker);
            }
        }
    }
}