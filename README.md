https://larevuedessinee.frama.io/

Ce site a pour objectif d'offrir une liste et une carte de tous les articles du magazine [La Revue Dessinée](https://www.larevuedessinee.fr).

🚧 👷‍♂️ : ce projet est en cours de développement, et vos contributions sont bienvenues, notamment dans le fichier de données `numeros.yaml` ! 😊

Numéros indexés : **8/45** ████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ 18%

## Outils employés
* [OSM Carto](https://wiki.openstreetmap.org/wiki/OpenStreetMap_Carto): _open-source stylesheet for rendering OpenStreetMap data (tile layer)_
* [Leaflet](https://leafletjs.com/): _an open-source JavaScript library  for mobile-friendly interactive maps_
* [leaflet-search](https://opengeo.tech/maps/leaflet-search/): _a Leaflet Control for search markers/features location by attribute_

## Lancement en local

    pip install -r requirements.txt
    ./gen.py --watch

## Sites de utils pour récupérer les descriptions d'articles
* <https://www.scopalto.com/revue/la-revue-dessinee>
* <https://www.bdphile.info/series/bd/13401-la-revue-dessinee>
* <https://clio-cr.clionautes.org/clio-pratique/revues/la-revue-dessinee>

## Reste à faire...
* [ ] boutons "S'abonner" -> ajouter "sur le site officiel"
* [ ] indiquer le nombre de numéros référencés sur le site
* [ ] ajouter un tableau type _DataTables_ de **TOUS** les articles, avec filtres de recherche
* [ ] ajouter un _tag-cloud_
* [ ] générer une page HTML par numéro
* [ ] ajouter des explications sur le fonctionnement du site, avec des flèches indiquant les champs de recherches
* [ ] ajouter un "guide du contributeur"
* [ ] ajouter **TOUS** les numéros dans `numeros.yaml`
